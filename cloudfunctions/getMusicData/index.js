/*
 * @Author: wanghuang
 * @Date: 2020-03-14 09:50:00
 * @LastEditTime: 2021-11-05 22:18:10
 * @LastEditors: wanghuang
 * @Description: 
 */
// 云函数入口文件
const cloud = require('wx-server-sdk')
const TcbRouter = require('tcb-router')
const axios = require('axios')
const BASE_URL = 'https://apis.imooc.com'
const ICODE = 'icode=E81B29DFA29E3EEF'

cloud.init()

// 云函数入口函数
exports.main = async(event, context) => {
  const app = new TcbRouter({
    event
  })
  // 读取歌单列表
  app.router('playList', async(ctx, next) => {
    ctx.body = await cloud.database()
      .collection('playList')
      .skip(event.start)
      .limit(event.count)
      .orderBy('createTime', 'desc')
      .get()
      .then(res => res)
  })
  // 读取歌单下的歌曲列表
  app.router('musicList', async(ctx, next) => {
      const res = await axios.get(`${BASE_URL}/playlist/detail?id=${parseInt(event.playListId)}&${ICODE}`)
      ctx.body = res.data
  })
  
  // 获取音乐播放地址
  app.router('musicUrl', async (ctx, next) => {
    const res = await axios.get(`${BASE_URL}/song/url?id=${event.musicId}&${ICODE}`)
    ctx.body = res.data
  })

  // * 获取歌词
  app.router('lyric', async (ctx, next) => {
    const res = await axios.get(`${BASE_URL}/lyric?id=${event.musicId}&${ICODE}`)
    ctx.body = res.data
  })

  return app.serve()
}