// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
const db = cloud.database() // 初始化云数据库
const axios = require('axios');
const URL = 'https://apis.imooc.com/personalized?icode=E81B29DFA29E3EEF'
const playListConnection = db.collection('playList')
const MAX_LIMIT = 10

// 云函数入口函数
exports.main = async (event, context) => {
  let originData = { // 从云数据库取到的原始数据
    data: []
  }
  const countObj = await playListConnection.count()
  const total = countObj.total
  const batchTimes = Math.ceil(total / MAX_LIMIT)
  const tasks = []
  for(let idx = 0; idx < batchTimes; idx++) {
    let promise = await playListConnection.skip(idx * MAX_LIMIT).limit(MAX_LIMIT).get()
    tasks.push(promise)
  }
  if(tasks.length) {
    originData = (await Promise.all(tasks)).reduce((acc, cur) => {
      return {
        data: [...acc.data, ...cur.data]
      }
    })
  }
  const { data } = await axios.get(URL);
  if (data.code === 1001) {
    console.log(data.msg);
    return 0;
  }
  const playList = data.result;
  // 当原始数据中存在数据时对比request数据，避免插入重复数据
  let newData = [];
  for (let i = 0, len1 = playList.length; i < len1; i++) {
    let flag = true
    for (let j = 0, len2 = originData.length; j < len2; j++) {
      if (playList[i].id === originData[j].id) {
        flag = false
        break
      }
    }
    if (flag) {
      let pl = playList[i]
      pl.createTime = db.serverDate()
      newData.push(pl)
    }
  }
  if (newData.length) {
    await playListConnection.add({
      data: newData
    }).then( res => {
      console.log('插入成功')
    }).catch( err => {
      console.log('插入失败')
    })
  }
  return newData.length
}