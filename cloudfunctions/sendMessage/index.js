const cloud = require('wx-server-sdk')
cloud.init()
exports.main = async (event, context) => {
  try {
    const wxContext = cloud.getWXContext()
    const templateId = 'TaS-dZUraVSJVg2CwdNeC-qtcP1GoH1q_ehKuxlgnI0'
    return await cloud.openapi.subscribeMessage.send({
      touser: wxContext.OPENID,
      page: `/pages/blog-comment/blog-comment?blogId=${event.blogId}`,
      lang: 'zh_CN',
      data: {
        name2: {
          value: event.nickName
        },
        thing3: {
          value: event.content
        }
      },
      templateId: templateId,
      miniprogramState: 'developer'
    })
    return result
  } catch (err) {
    return err
  }
}
