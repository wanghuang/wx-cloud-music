const app = getApp()
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    musicList: Array
  },

  /**
   * 组件的初始数据
   */
  data: {
    musicId: -1
  },
  pageLifetimes: {
    show() {
      this.setData({
        musicId: parseInt(app.getPlayMusicId())
      })

    }
  },
  /**
   * 组件的方法列表
   */
  methods: {
    handleSelect(event) {
      const { musicid, index } = event.currentTarget.dataset
      this.setData({ musicId: musicid })
      wx.navigateTo({
        url: `../../pages/player/index?musicId=${musicid}&index=${index}`,
      })
    }
  }
})
