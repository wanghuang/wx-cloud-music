let movableAreaWidth = 0
let movableViewWidth = 0
const backgroundAudioManager = wx.getBackgroundAudioManager()
let currentSec = -1 // 当前的秒数
let duration = 0 // 当前歌曲的总时长，以秒为单位
let isMove = false // * 当前进度条是否在拖拽

Component({
  /**
   * 组件的属性列表
   */
  properties: {
    isSame: Boolean
  },

  /**
   * 组件的初始数据
   */
  data: {
    showTime: {
      currentTime: '00:00',
      totalTime: '00:00',
    },
    movableDis: 0,
    progress: 0
  },

  lifetimes: {
    ready() {
      this.setTime()
      this.getMovableWidth()
      this.bindBGMEvent()
    },
  },

  /**
   * 组件的方法列表
   */
  methods: {
    onChange(event) {
      const { detail } = event
      if(detail.source === 'touch') {
        this.data.progress = detail.x / (movableAreaWidth - movableViewWidth) * 100
        this.data.movableDis = detail.x
        isMove = true
      }
    },
    onTouchEnd() {
      const currentTime = Math.floor(backgroundAudioManager.currentTime)
      const currentTimeFmt = this.dateFormate(currentTime)
      this.setData({
        progress: this.data.progress,
        movableDis: this.data.movableDis,
        ['showTime.currentTime']: currentTimeFmt.min + ':' + currentTimeFmt.sec
      })
      // * 设置音乐播放时刻(秒)
      backgroundAudioManager.seek(duration * this.data.progress / 100)
      isMove = false
    },
    setTime() {
      duration = backgroundAudioManager.duration
      const durationFmt = this.dateFormate(duration)
      this.setData({
        ['showTime.totalTime']: `${durationFmt.min}:${durationFmt.sec}`
      })
    },
    dateFormate(sec) {
      const min = Math.floor(sec / 60)
      sec = Math.floor(sec % 60)
      return {
        'min': this.parse0(min),
        'sec': this.parse0(sec),
      }
    },
    getMovableWidth() {
      const query = this.createSelectorQuery()
      query.select('.movable-area').boundingClientRect()
      query.select('.movable-view').boundingClientRect()
      query.exec((rect) => {
        movableAreaWidth = rect[0].width
        movableViewWidth = rect[1].width
        console.log(movableAreaWidth, movableViewWidth)
      })

    },
    bindBGMEvent() {
      backgroundAudioManager.onPlay(() => {
        console.log('onPlay')
        isMove = false
        this.triggerEvent('musicPlay')
      })

      backgroundAudioManager.onStop(() => {
        console.log('onStop')
      })

      backgroundAudioManager.onPause(() => {
        console.log('Pause')
        this.triggerEvent('musicPause')
      })

      backgroundAudioManager.onWaiting(() => {
        console.log('onWaiting')
      })

      backgroundAudioManager.onCanplay(() => {
        console.log('onCanplay')
        if (typeof backgroundAudioManager.duration != 'undefined') {
          this.setTime()
        } else {
          setTimeout(() => {
            this.setTime()
          }, 1000)
        }
      })

      backgroundAudioManager.onTimeUpdate(() => {
        // * 进度条在拖拽时，阻止使用拖拽产生的progress和movableDis
        if(!isMove) {
          const { currentTime, duration } = backgroundAudioManager
          const gotSecond = currentTime.toString().split(',')[0]
          if (gotSecond !== currentSec) {
            const currentTimeFmt = this.dateFormate(currentTime)
            this.setData({
              movableDis: (movableAreaWidth - movableViewWidth) * currentTime / duration,
              progress: currentTime / duration * 100,
              ['showTime.currentTime']: `${currentTimeFmt.min}:${currentTimeFmt.sec}`,
            })
            currentSec = gotSecond
            this.triggerEvent('musicTimeUpdate', { currentTime })
          }
        }
      })

      backgroundAudioManager.onEnded(() => {
        console.log("onEnded")
        this.triggerEvent('musicEnded')
      })

      backgroundAudioManager.onError((res) => {
        wx.showToast({
          title: '错误:' + res.errCode,
        })
      })
    },
    // 补零
    parse0(sec) {
      return sec < 10 ? '0' + sec : sec
    }
  }
})
