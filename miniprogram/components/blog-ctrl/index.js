let userInfo = {}
let content = ''
const db = wx.cloud.database()
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    blogId: String,
    blog: Object
  },
  externalClasses: ['iconfont', 'iconcomment', 'iconshare'],
  /**
   * 组件的初始数据
   */
  data: {
    authorizeShow: false, //* 授权弹框组件
    showModal: false, //* 底部弹框组件
    content: '',
  },

  /**
   * 组件的方法列表
   */
  methods: {
    onComment() {
      wx.getSetting({
        success: res => {
          if (res.authSetting['scope.userInfo']) {
            wx.getUserInfo({
              success: res => {
                userInfo = res.userInfo
                // 显示评论弹框
                this.setData({
                  showModal: true,
                })
              }
            })
          } else {
            this.setData({
              authorizeShow: true,
            })
          }
        }
      })
    },
    authorizeSuccess(event) {
      userInfo = event.detail
      // 授权框消失，评论框显示
      this.setData({
        authorizeShow: false,
      }, () => {
        this.setData({
          showModal: true,
        })
      })
    },

    authorizedFail() {
      wx.showModal({
        title: '授权用户才能进行评价哦',
        content: '',
      })
    },
    handleInput(event) {
      content = event.detail.value
    },
    onSend() {
      wx.requestSubscribeMessage({
        tmplIds: ['TaS-dZUraVSJVg2CwdNeC-qtcP1GoH1q_ehKuxlgnI0'],
        success(res) {
          if (res['TaS-dZUraVSJVg2CwdNeC-qtcP1GoH1q_ehKuxlgnI0'] == 'accept') {
            //用户同意了订阅，允许订阅消息
            wx.showToast({
              title: '订阅成功'
            })
          } else {
            //用户拒绝了订阅，禁用订阅消息
            wx.showToast({
              title: '订阅失败'
            })
          }
        },
        fail(err) {
          console.error(err)
        }
      })
      // 插入数据库
      if (content.trim() == '') {
        wx.showModal({
          title: '评论内容不能为空',
          content: '',
        })
        return
      }
      wx.showLoading({
        title: '评论中',
        mask: true,
      })
      db.collection('blog-comment').add({
        data: {
          content,
          createTime: db.serverDate(),
          blogId: this.properties.blogId,
          nickName: userInfo.nickName,
          avatarUrl: userInfo.avatarUrl
        }
      }).then((res) => {
        // 推送模板消息
        wx.cloud.callFunction({
          name: 'sendMessage',
          data: {
            content,
            nickName: userInfo.nickName,
            blogId: this.properties.blogId
          }
        }).then((res) => {
          console.log(res)
        })
        wx.hideLoading()
        wx.showToast({
          title: '评论成功',
        })
        this.setData({
          showModal: false,
          content: '',
        })
        // 父元素刷新评论页面
        this.triggerEvent('refreshCommentList')
      })
    }
  }
})