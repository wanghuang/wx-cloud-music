// components/playListItem/index.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    songObj: {
      type: Object
    }
  },
  observers: {
    ['songObj.playCount'](count) {
      this.setData({
        count: this.formateCount(count,2)
      })
    }
  },
  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    toMdusicDetail() {
      wx.navigateTo({
        url: `../../pages/musicList/index?playListId=${this.properties.songObj.id}`,
      })
    },
    formateCount(num,point) {
      let numStr = num.toString().split('.')[0]
      if (numStr.length < 6) {
        return numStr
      } else if (numStr.length >= 6 && numStr.length <= 8) {
        let decimal = numStr.substring(numStr.length - 4, numStr.length - 4 + point)
        return parseFloat(parseInt(num / 10000) + '.' + decimal) +
          '万'
      } else if (numStr.length > 8) {
        let decimal = numStr.substring(numStr.length - 8, numStr.length - 8 + point)
        return parseFloat(parseInt(num / 100000000) + '.' + decimal) + '亿'
      }
    }
  }
})
