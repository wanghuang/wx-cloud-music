// pages/player/index.js
let currenIndex = 0;
let musicList = [];
const backgroundAudioManager = wx.getBackgroundAudioManager()
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    isPlaying: false,
    showLyric: false, //* 是否显示歌词
    lyric: '', //* 歌词数据
    isSame: false //* 是否同一首歌
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    const { musicId, index } = options
    currenIndex = index
    musicList = wx.getStorageSync('musicList')
    this.loadMusicDetail(musicId)
  },
  loadMusicDetail(musicId) {
    let musicDetail = musicList[currenIndex]
    let same = app.getPlayMusicId() == musicId 
    wx.setNavigationBarTitle({
      title: musicDetail.name,
    })
    this.setData({
      picUrl: musicDetail.al.picUrl
    })
    !same && backgroundAudioManager.stop()
    wx.showLoading({
      title: '正在加载歌曲信息...',
    })
    wx.cloud.callFunction({
      name: 'getMusicData',
      data: {
        musicId,
        $url: 'musicUrl'
      }
    }).then(res => {
      console.log('有什么问题呢',res)
      const result = res.result
      if (result.data[0].url == null) {
        wx.showToast({
          title: '木有VIP呀~',
          duration: 2000
        })
        this.handleNext()
        return
      }
      if (!same) {
        backgroundAudioManager.src = result.data[0].url
        backgroundAudioManager.title = musicDetail.name
        // backgroundAudioManager.coverImgUrl = musicDetail.al.picUrl
        // backgroundAudioManager.singer = musicDetail.ar[0].name
        // backgroundAudioManager.epname = musicDetail.al.name
        this.savePlayHistory()
      }
      this.setData({
        isPlaying: true,
        isSame: same
      })
      wx.hideLoading()

      // * 加载歌词
      wx.cloud.callFunction({
        name: 'getMusicData',
        data: {
          musicId,
          $url: 'lyric'
        }
      }).then(res => {
        let lyric = '暂无歌词'
        const lrc = res.result.lrc
        lrc && (lyric = lrc.lyric)
        this.setData({
          lyric
        })
      })
      app.setPlayMusicId(musicId)
    })

  },
  changePlayStatus() {
    if (this.data.isPlaying) {
      backgroundAudioManager.pause()
    } else {
      backgroundAudioManager.play()
    }
    this.setData({
      isPlaying: !this.data.isPlaying
    })
  },
  handlePrev() {
    currenIndex--;
    if (currenIndex < 0) currenIndex = musicList.length - 1
    this.loadMusicDetail(musicList[currenIndex].id)
  },
  handleNext() {
    currenIndex++;
    if (currenIndex >= musicList.length) currenIndex = 0
    this.loadMusicDetail(musicList[currenIndex].id)
  },
  // * 封面与歌词组件切换
  coverLyricSwitch() {
    this.setData({
      showLyric: !this.data.showLyric
    })
  },
  // * 监听progress-bar组件获取当前播放时间
  musicTimeUpdate(event) {
    this.selectComponent('.lyric').update(event.detail.currentTime)
  },
  sysControlPlay() {
    this.setData({
      isPlaying: true
    })
  },
  sysControlPause() {
    this.setData({
      isPlaying: false
    })
  },
  savePlayHistory() {
    //  当前正在播放的歌曲
    const music = musicList[currenIndex]
    const openid = app.globalData.openid
    const historhMusicList = wx.getStorageSync(openid)
    let bHave = false
    for (let i = 0, len = historhMusicList.length; i < len; i++) {
      if (historhMusicList[i].id == music.id) {
        bHave = true
        break
      }
    }
    if (!bHave) {
      historhMusicList.unshift(music)
      wx.setStorage({
        key: openid,
        data: historhMusicList,
      })
    }
  },
})