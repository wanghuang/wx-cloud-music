// pages/blog/blog.js
let keyword = '';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    showModal: false,
    blogList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.getBloglist()
  },

  onPublish() {
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          wx.getUserInfo({
            success: result => {
              this.handleAuthorizeSuccess({
                detail: result.userInfo
              })
            }
          })
        } else {
          this.setData({
            showModal: true
          })
        }
      }
    })
  },
  handleAuthorizeSuccess(event) {
    const {
      detail
    } = event
    wx.navigateTo({
      url: `../blog-edit/blog-edit?nickName=${detail.nickName}&avatarUrl=${detail.avatarUrl}`,
    })
  },
  handleAuthorizeFail() {
    wx.showModal({
      title: '授权后才能发布哟~',
      content: '',
    })
  },
  getBloglist(start = 0) {
    wx.showLoading({
      title: '拼命加载中',
    })
    wx.cloud.callFunction({
      name: 'blog',
      data: {
        keyword,
        start,
        count: 10,
        $url: 'list',
      }
    }).then((res) => {
      this.setData({
        blogList: this.data.blogList.concat(res.result)
      })
      wx.hideLoading()
      wx.stopPullDownRefresh()
    })
  },
  onPullDownRefresh: function() {
    this.setData({
      blogList: []
    })
    this.getBloglist(0)
  },
  onReachBottom: function() {
    this.getBloglist(this.data.blogList.length)
  },

  goComment(event) {
    wx.navigateTo({
      url: '../../pages/blog-comment/blog-comment?blogId=' + event.target.dataset.blogid,
    })
  },
  handleSearch(evt) {
    keyword = evt.detail.inputText
    this.setData({
      blogList: []
    })
    this.getBloglist()
  },
  onShareAppMessage: function (event) {
    let blogObj = event.target.dataset.blog
    return {
      title: blogObj.content,
      path: `/pages/blog-comment/blog-comment?blogId=${blogObj._id}`,
      // imageUrl: ''
    }
  }
})