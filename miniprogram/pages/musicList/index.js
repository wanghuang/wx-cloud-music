/*
 * @Author: wanghuang
 * @Date: 2020-03-14 14:31:50
 * @LastEditTime: 2021-11-05 22:20:46
 * @LastEditors: wanghuang
 * @Description: 
 */
// miniprogram/pages/musicDetail/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    musicList: [], // 歌曲列表
    musicListInfo: {} // 封面信息
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showLoading({
      title: '加载中...',
    })
    wx.cloud.callFunction({
      name: 'getMusicData',
      data: {
        playListId: options.playListId,
        $url: 'musicList'
      }
    }).then( res => {
      const { playlist } = res.result
      this.setData({
        musicList: playlist.tracks,
        musicListInfo: {
          coverImgUrl: playlist.coverImgUrl,
          name: playlist.name
        }
      })
      this.setMusicList()
      wx.hideLoading()
    })
  },
  setMusicList() {
    wx.setStorageSync('musicList', this.data.musicList)
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})